#!/bin/bash

function collectStandardSystemDetails {
    printf "*** System Details ***\n"
    uname -a

    printf "\n"
    cat /etc/os-release

    printf "\n"
    cat /proc/cpuinfo | grep -i "model name"

    printf "\n*** Memory Usage ***\n"
    free -m
}

function collectNetworkDetails {
    printf "\n*** Interface Details ***\n"
    ip a
    netstat -i

    printf "\n*** Routing Details ***\n"
    netstat -nr
}

collectStandardSystemDetails
collectNetworkDetails